-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 15 mai 2018 à 08:48
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sia`
--

-- --------------------------------------------------------

--
-- Structure de la table `author`
--

DROP TABLE IF EXISTS `author`;
CREATE TABLE IF NOT EXISTS `author` (
  `id_author` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(50) DEFAULT NULL,
  `fullName` varchar(2000) DEFAULT NULL,
  `firstName` varchar(200) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `bio` varchar(100) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  PRIMARY KEY (`id_author`),
  UNIQUE KEY `fullName` (`fullName`)
) ENGINE=InnoDB AUTO_INCREMENT=153287 DEFAULT CHARSET=latin1;

--
-- Déclencheurs `author`
--
DROP TRIGGER IF EXISTS `update_auth_id`;
DELIMITER $$
CREATE TRIGGER `update_auth_id` AFTER INSERT ON `author` FOR EACH ROW IF (SELECT lname FROM users WHERE SUBSTRING_INDEX(NEW.fullName, ' ', -1 ) = users.lname) is not null THEN
UPDATE users 
       SET users.ID_SEARCHER = NEW.id_author 
       WHERE users.fname = SUBSTRING_INDEX(NEW.fullName, ' ', 1 )
       AND users.lname = SUBSTRING_INDEX(NEW.fullName, ' ', -1 );
END IF
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `update_name`;
DELIMITER $$
CREATE TRIGGER `update_name` BEFORE INSERT ON `author` FOR EACH ROW BEGIN
         SET NEW.firstName = SUBSTRING_INDEX(NEW.fullName, ' ', 1 );
         SET NEW.lastName = SUBSTRING_INDEX(NEW.fullName, ' ', -1 );
         SET NEW.id = CONCAT(SUBSTRING_INDEX(NEW.fullName, ' ', 1 ), LEFT(SUBSTRING_INDEX(NEW.fullName, ' ', -1 ),2), YEAR(CURRENT_TIME()));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `contribution`
--

DROP TABLE IF EXISTS `contribution`;
CREATE TABLE IF NOT EXISTS `contribution` (
  `id_author` int(11) DEFAULT NULL,
  `fullName` varchar(1000) NOT NULL,
  `id_publication` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_publication`,`fullName`) USING BTREE,
  KEY `FK_AUTHOR` (`id_author`),
  KEY `FK_NAME` (`fullName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `contribution`
--
DROP TRIGGER IF EXISTS `update_id_auther`;
DELIMITER $$
CREATE TRIGGER `update_id_auther` BEFORE INSERT ON `contribution` FOR EACH ROW IF NEW.id_author is null THEN 
  SET NEW.id_author = (SELECT author.id_author from author WHERE author.fullName = NEW.fullName);
  END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `data`
--

DROP TABLE IF EXISTS `data`;
CREATE TABLE IF NOT EXISTS `data` (
  `id_data` varchar(20) NOT NULL,
  `dataName` varchar(20) NOT NULL,
  `dataType` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL,
  `producedDate` date NOT NULL,
  `ID_PUBLICATION` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_data`),
  KEY `FK_PUBLICATION_DATA` (`ID_PUBLICATION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `experiment`
--

DROP TABLE IF EXISTS `experiment`;
CREATE TABLE IF NOT EXISTS `experiment` (
  `id_experiment` varchar(20) NOT NULL,
  `hypothesis` varchar(100) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `conclusion` varchar(100) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ID_PUBLICATION` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_experiment`),
  KEY `FK_PUBLICATION_EXPERIMENT` (`ID_PUBLICATION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `id_publication` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `pages` varchar(1000) DEFAULT NULL,
  `year` varchar(11) DEFAULT NULL,
  `crossref` varchar(1000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `ee` varchar(1000) DEFAULT NULL,
  `publisher` varchar(1000) DEFAULT NULL,
  `isbn` varchar(1000) DEFAULT NULL,
  `series` varchar(1000) DEFAULT NULL,
  `booktitle` varchar(1000) DEFAULT NULL,
  `journal` varchar(1000) DEFAULT NULL,
  `volume` varchar(1000) DEFAULT NULL,
  `cdrom` varchar(1000) DEFAULT NULL,
  `number` varchar(20) DEFAULT NULL,
  `mdate` varchar(20) DEFAULT NULL,
  `pubkey` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_publication`),
  UNIQUE KEY `pubkey_2` (`pubkey`),
  KEY `id_publication` (`id_publication`),
  KEY `pubkey` (`pubkey`)
) ENGINE=InnoDB AUTO_INCREMENT=64299 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  `login` varchar(50) DEFAULT NULL,
  `ID_SEARCHER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `FK_AUTH` (`ID_SEARCHER`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déclencheurs `users`
--
DROP TRIGGER IF EXISTS `update_id_auth`;
DELIMITER $$
CREATE TRIGGER `update_id_auth` BEFORE INSERT ON `users` FOR EACH ROW IF NEW.ID_SEARCHER is null THEN 
  SET NEW.ID_SEARCHER = (SELECT author.id_author from author WHERE author.firstName = NEW.fname and author.lastName = NEW.lname);
  END IF
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `update_user_log`;
DELIMITER $$
CREATE TRIGGER `update_user_log` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
         SET NEW.login = CONCAT(NEW.fname, NEW.lname, YEAR(CURRENT_TIME()));
END
$$
DELIMITER ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `contribution`
--
ALTER TABLE `contribution`
  ADD CONSTRAINT `FK_AUTHOR` FOREIGN KEY (`id_author`) REFERENCES `author` (`id_author`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_NAME` FOREIGN KEY (`fullName`) REFERENCES `author` (`fullName`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PUBLICATION` FOREIGN KEY (`id_publication`) REFERENCES `publication` (`pubkey`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `data`
--
ALTER TABLE `data`
  ADD CONSTRAINT `FK_PUBLICATION_DATA` FOREIGN KEY (`ID_PUBLICATION`) REFERENCES `publication` (`id_publication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `experiment`
--
ALTER TABLE `experiment`
  ADD CONSTRAINT `FK_PUBLICATION_EXPERIMENT` FOREIGN KEY (`ID_PUBLICATION`) REFERENCES `publication` (`id_publication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ID_SEARCHER`) REFERENCES `author` (`id_author`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
