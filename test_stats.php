<?php


$xml = simplexml_load_file("sia.xml");

$con = mysqli_connect('localhost', 'root', '', 'sia');


// Check connection
if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
} 


$sql = "SELECT sum_auth.somme/count(DISTINCT c.id_publication) as moyenne
FROM (SELECT sum(nb_auth.nb_group) as somme 
FROM(SELECT *, count(c.id_author) as nb_group from contribution c group by c.id_publication) as nb_auth) sum_auth, contribution c";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br> Moyenne du nombre : ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/

$sql = "SELECT pp.type, group_type_auth.group_auth/COUNT(DISTINCT pp.pubkey) as moyenne_par_type
FROM (SELECT p.type, sum_auth.*, sum(sum_auth.nb_group) as group_auth      
FROM(SELECT id_publication, count(c.id_author) as nb_group from contribution c group by c.id_publication) as sum_auth, publication p
where sum_auth.id_publication = p.pubkey
group by p.type) group_type_auth, publication pp
where group_type_auth.type = pp.type
group by pp.type";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br> ". " Type : ". $row["type"]. " - Moyenne du nombre d'auteur : ". $row["moyenne_par_type"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/

$sql = "SELECT sum_auth.somme/count(DISTINCT c.id_author) as moyenne
FROM (SELECT sum(nb_auth.nb_group) as somme
FROM(SELECT *, count(c.id_publication) as nb_group from contribution c group by c.id_author) as nb_auth) sum_auth, contribution c";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br> Moyenne du nombre d'article par auteur : ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/

$sql = "SELECT pp.type, sum(pp.nbpub) /count(DISTINCT pp.id_author) as moyenne
from (SELECT p.type,c.id_author, count(c.id_publication) as nbpub
From publication p,contribution c
where c.id_publication = p.pubkey
group by p.type,c.id_author) pp
group by pp.type";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br> ". " Type : ". $row["type"]. " - Moyenne du nombre d'article par auteur par type : ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/
$sql = "SELECT pub_year.somme /count_year.count_dis_yaer as moyenne
FROM (
SELECT sum(nb_pub_year.nb_group) as somme
FROM (SELECT year, count(p.pubkey) as nb_group from publication p group by p.year) as nb_pub_year) pub_year, (SELECT count(DISTINCT publication.year) as count_dis_yaer FROM publication) count_year";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br> Moyenne du nombre d'article par année: ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/
$sql = "SELECT type, pub_year.somme /count_year.count_dis_yaer as moyenne
FROM (
SELECT type, sum(nb_pub_year.nb_group) as somme
FROM (SELECT type, year, count(p.pubkey) as nb_group from publication p group by p.type, p.year) as nb_pub_year
group by type) pub_year, (SELECT YEAR(CURRENT_DATE) - MIN(year) as count_dis_yaer FROM publication) count_year
group by type";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br>". " Type : ". $row["type"]. " - Moyenne du nombre d'article par année: ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/

$sql = "SELECT sum(pp.nbauth)/(YEAR(CURRENT_DATE) - min(pp.year)) as moyenne
From (Select p.year ,count(DISTINCT c.id_author) as nbauth
From publication p,contribution c
where c.id_publication = p.pubkey
group by p.year) pp";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br> Moyenne du nombre d'auteur ayant publié sur une année: ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/
$sql = "SELECT pp.type,sum(pp.nbauth)/(YEAR(CURRENT_DATE) - min(pp.year)) as moyenne
From (Select p.year,p.type,count(DISTINCT c.id_author) as nbauth
From publication p,contribution c
where c.id_publication = p.pubkey
group by p.year,p.type) pp
group by pp.type";

$result = $con->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<br>". " Type : ". $row["type"]. " - Moyenne du nombre d'auteur ayant publié sur une année: ". $row["moyenne"]. "<br>";
    }
} else {
    echo "0 results";
}
echo "===================================================================";
/*=======================================================================*/
$con->close();
?> 
