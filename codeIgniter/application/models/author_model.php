<?php
/*
 * File Name: publication_model.php
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class author_model extends CI_Model
{
    function __construct()
    {
        //Call the Model constructor
        parent::__construct();
    }

    //fetch all publication records
//    function get_publication_list()
//    {
//        $this->db->from('publication');
//        $query = $this->db->get();
//        return $query->result();
//    }


    // Fetch records
    public function get_author($searchAuthor) {

        $query = $this->db->query("SELECT * FROM author");
        $this->db->like('contribution.fullName', $searchAuthor);

        return $query->result();

    }

    public function get_author_pub($id) {

        $this->db->select('publication.title');
        $this->db->from('contribution');
        $this->db->join('publication', 'contribution.id_publication = publication.pubkey');
        $this->db->join('author', 'author.fullName = contribution.fullName');
        $this->db->where('author.id_author', $id);

        $query = $this->db->get();

        return $query->result();

    }

    public function get_author_list() {

        $this->db->select('*');
        $this->db->from('author');

        $query = $this->db->get();
        return $query->result();

    }

    function get_author_by_id($id)
    {
        $this->db->where('id_author', $id);
        $query = $this->db->get('author');
        return $query->result();
    }
}
?>
