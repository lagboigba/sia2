<?php
/*
 * File Name: publication_model.php
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class publication_model extends CI_Model
{
    function __construct()
    {
        //Call the Model constructor
        parent::__construct();
    }

    //fetch all publication records
//    function get_publication_list()
//    {
//        $this->db->from('publication');
//        $query = $this->db->get();
//        return $query->result();
//    }


    // Fetch records
    public function get_publication_list($search="",$searchAuthor="",$searchTitle="") {

       $this->db->select('*');
       $this->db->from('publication');
       $this->db->join('contribution', 'contribution.id_publication = publication.pubkey');
       $this->db->join('author', 'author.fullName = contribution.fullName');


        if($search != ''){
            $this->db->like('type', $search);
        }

        if($searchAuthor != ''){
            $this->db->like('contribution.fullName', $searchAuthor);
        }

        if($searchTitle != ''){
            $this->db->like('publication.title', $searchTitle);
        }

        $query = $this->db->get();

        return $query->result();

    }

    function get_publication_by_pubkey($pubkey)
    {
        $this->db->where('pubkey', $pubkey);
        $query = $this->db->get('publication');
        return $query->result();
    }


}
?>
