<?php
/*
 * File Name: publication_model.php
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stats_model extends CI_Model
{
    function __construct()
    {
        //Call the Model constructor
        parent::__construct();
    }

    //fetch all publication records
//    function get_publication_list()
//    {
//        $this->db->from('publication');
//        $query = $this->db->get();
//        return $query->result();
//    }


    // Fetch records
    public function get_mean_publication() {

    $query = $this->db->query("SELECT sum_auth.somme/count(DISTINCT c.id_publication) as moyenne
    FROM (SELECT sum(nb_auth.nb_group) as somme 
    FROM(SELECT *, count(c.id_author) as nb_group from contribution c group by c.id_publication) as nb_auth) sum_auth, contribution c");

        return $query->result();

    }

    public function get_mean_publication_type() {

    $query = $this->db->query("SELECT pp.type, group_type_auth.group_auth/COUNT(DISTINCT pp.pubkey) as moyenne_par_type
FROM (SELECT p.type, sum_auth.*, sum(sum_auth.nb_group) as group_auth      
FROM(SELECT id_publication, count(c.id_author) as nb_group from contribution c group by c.id_publication) as sum_auth, publication p
where sum_auth.id_publication = p.pubkey
group by p.type) group_type_auth, publication pp
where group_type_auth.type = pp.type
group by pp.type");

        return $query->result();

    }

        public function get_mean_publication_author() {

    $query = $this->db->query("SELECT sum_auth.somme/count(DISTINCT c.id_author) as moyenne
FROM (SELECT sum(nb_auth.nb_group) as somme
FROM(SELECT *, count(c.id_publication) as nb_group from contribution c group by c.id_author) as nb_auth) sum_auth, contribution c");

        return $query->result();

    }

        public function get_mean_publication_author_type() {

    $query = $this->db->query("SELECT pp.type, sum(pp.nbpub) /count(DISTINCT pp.id_author) as moyenne
from (SELECT p.type,c.id_author, count(c.id_publication) as nbpub
From publication p,contribution c
where c.id_publication = p.pubkey
group by p.type,c.id_author) pp
group by pp.type");

        return $query->result();

    }

        public function get_mean_publication_year() {

    $query = $this->db->query("SELECT pub_year.somme /count_year.count_dis_yaer as moyenne
FROM (
SELECT sum(nb_pub_year.nb_group) as somme
FROM (SELECT year, count(p.pubkey) as nb_group from publication p group by p.year) as nb_pub_year) pub_year, (SELECT count(DISTINCT publication.year) as count_dis_yaer FROM publication) count_year");

        return $query->result();

    }

        public function get_mean_publication_year_type() {

    $query = $this->db->query("SELECT type, pub_year.somme /count_year.count_dis_yaer as moyenne
FROM (
SELECT type, sum(nb_pub_year.nb_group) as somme
FROM (SELECT type, year, count(p.pubkey) as nb_group from publication p group by p.type, p.year) as nb_pub_year
group by type) pub_year, (SELECT YEAR(CURRENT_DATE) - MIN(year) as count_dis_yaer FROM publication) count_year
group by type");

        return $query->result();

    }

        public function get_mean_publication_year_auth() {

    $query = $this->db->query("SELECT sum(pp.nbauth)/(YEAR(CURRENT_DATE) - min(pp.year)) as moyenne
From (Select p.year ,count(DISTINCT c.id_author) as nbauth
From publication p,contribution c
where c.id_publication = p.pubkey
group by p.year) pp");

        return $query->result();

    }

        public function get_mean_publication_year_auth_type() {

    $query = $this->db->query("SELECT pp.type,sum(pp.nbauth)/(YEAR(CURRENT_DATE) - min(pp.year)) as moyenne
From (Select p.year,p.type,count(DISTINCT c.id_author) as nbauth
From publication p,contribution c
where c.id_publication = p.pubkey
group by p.year,p.type) pp
group by pp.type");

        return $query->result();

    }

}
?>
