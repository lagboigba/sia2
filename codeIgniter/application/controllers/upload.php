<?php

//Classe utilisée pour importer des fichiers
class Upload extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
  }

  public function index($publication)
  {
    $this->load->view('upload_form', array('error' => ' ', 'test' => $publication));
  }

  public function do_upload($publication)
  {
    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'gif|jpg|png|pdf';
    $config['file_name'] = $publication;


    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('userfile'))
    {
      $error = array('error' => $this->upload->display_errors());

      $this->load->view('upload_form', $error);
    }
    else
    {
      $data = array('upload_data' => $this->upload->data());


      $this->load->view('upload_success', $data);
    }
  }
  //fonction pour visualiser
  public function displaypdf($name){

    $string_val = "Content-Disposition: inline; filename=".$name;
    $upload_path = 'uploads/' . $name;
    header("Content-type: application/pdf");

    header($string_val);
    @readfile($upload_path);
  }

}
?>