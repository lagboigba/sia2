<?php
class author extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','html'));
        $this->load->library('session');
        $this->load->database();
        $this->load->model('author_model');
    }

//    function index()
//    {
//        $details = $this->publication_model->get_publication_by_id(65230);
//        $data['publication_id'] = $details[0]->publication_id;
//        $data['type'] = $details[0]->type;
//        $data['url'] = $details[0]->url;
//        $this->load->view('publication_view',$data);
//    }
//
    function display($id){
        $details = $this->author_model->get_author_by_id($id);
        $data['id'] = $details[0]->id;
        $data['firstName'] = $details[0]->firstName;
        $data['lastName'] = $details[0]->lastName;

        //autres données

        //contrib
        $data['contributions'] = $this->author_model->get_author_pub($id);
        //var_dump($data);

        $this->load->view('author_view',$data);

    }


}