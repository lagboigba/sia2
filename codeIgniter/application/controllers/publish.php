<?php
/*
 * File Name: publish.php
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class publish extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('user_model');
        $this->load->model('author_model');
        //load the employee model
        //$this->load->model('employee_model');
    }

    //index function
    function index()
    {
        //fetch data from department and designation tables
        //implique d'avoir une liste prédéfinie de domaine ancrée dans la bdd
        //$data['department'] = $this->employee_model->get_department();
        //$data['designation'] = $this->employee_model->get_designation();

        //set validation rules
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('pages', 'Number of pages', 'required|numeric');
        $this->form_validation->set_rules('year', 'Year', 'required|numeric');
        $this->form_validation->set_rules('crossref', 'Crossref');
        $this->form_validation->set_rules('url', 'URL');
        $this->form_validation->set_rules('ee', 'EE');
        $this->form_validation->set_rules('publisher', 'Publisher');
        $this->form_validation->set_rules('isbn', 'ISBN');
        $this->form_validation->set_rules('series', 'Series');
        $this->form_validation->set_rules('booktitle', 'Booktitle');
        $this->form_validation->set_rules('journal', 'Journal');
        $this->form_validation->set_rules('volume', 'Volume');
        $this->form_validation->set_rules('cdrom', 'CDROM');
        $this->form_validation->set_rules('number', 'Number', 'numeric');
        $this->form_validation->set_rules('mdate', 'mdate'); //ajouter date only
        $this->form_validation->set_rules('pubkey', 'Publication Key');


        if ($this->form_validation->run() == FALSE)
        {
            //fail validation
            $this->load->view('publish_view');
        }
        else
        {
            //pass validation
            $data = array(
                'type' => $this->input->post('type'),
                'title' => $this->input->post('title'),
                'pages' => $this->input->post('pages'),
                'year' => $this->input->post('year'),
                'crossref' => $this->input->post('crossref'),
                'url' => $this->input->post('url'),
                'ee' => $this->input->post('ee'),
                'publisher' => $this->input->post('publisher'),
                'isbn' => $this->input->post('isbn'),
                'series' => $this->input->post('series'),
                'booktitle' => $this->input->post('booktitle'),
                'journal' => $this->input->post('journal'),
                'volume' => $this->input->post('volume'),
                'cdrom' => $this->input->post('cdrom'),
                'number' => $this->input->post('number'),
                'mdate' => $this->input->post('mdate'),
                'pubkey' => $this->input->post('pubkey'),


            );

            $details = $this->user_model->get_user_by_id($this->session->userdata('uid'));
            $data3 = array(
            'firstName' => $details[0]->fname,
            'lastName' => $details[0]->lname,
            'fullName' => $details[0]->fname . " " . $details[0]->lname,
        );
           $data4 = array(
               'fullName'  => $details[0]->fname . " " . $details[0]->lname,
               'id_publication' => $this->input->post('pubkey'),
           );



//            $data2 = array(
//                'dataName' => $this->input->post('data'),
//                'ID_PUBLICATION' => $this->input->post('pubkey'),
//        );

            //insert the form data into database
            $this->db->insert('publication', $data);
            //$this->db->insert('data', $data2);
        $details_auth = $this->author_model->get_author_by_id($details[0]->fname . " " . $details[0]->lname);

            //if  ($details_auth[0]->firstName != $details[0]->fname || $details_auth[0]->lastName != $details[0]->lname ){
            if (!empty($details_auth)){
            $this->db->insert('author', $data3);
        }
            $this->db->insert('contribution', $data4);


//            $data2 = array(
//                'id' => ($this->session->userdata('uid')),
//                'id_Article' => $this->input->post('id_Article'),
//            );
//
//
//            $this->db->insert('contribution', $data2);



            //display success message
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Publication details added to Database!!!</div>');
            //redirect('publish/index');
            redirect('upload/index/'.  $this->input->post('pubkey'));
        }

    }

    //custom validation function for dropdown input
    function combo_check($str)
    {
        if ($str == '-SELECT-')
        {
            $this->form_validation->set_message('combo_check', 'Valid %s Name is required');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //custom validation function to accept only alpha and space input
    function alpha_only_space($str)
    {
        if (!preg_match("/^([-a-z ])+$/i", $str))
        {
            $this->form_validation->set_message('alpha_only_space', 'The %s field must contain only alphabets or spaces');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }


}


?>