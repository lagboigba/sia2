<?php
/*
 * File Name: deleteemployee.php
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class displaypublications extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        //load the article model
        $this->load->model('publication_model');
        $this->load->model('author_model');
        $this->load->library('unit_test');

    }

    //index function
    function index()
    {
        //get the employee list
        $search_text = "";
        $search_text_author = "";
        $search_text_title = "";
        if($this->input->post('submit') != NULL ){
            $search_text = $this->input->post('search');
            $search_text_author = $this->input->post('searchAuthor');
            $search_text_title = $this->input->post('searchTitle');
            $this->session->set_userdata(array("search"=>$search_text));
            $this->session->set_userdata(array("searchAuthor"=>$search_text_author));
            $this->session->set_userdata(array("searchTitle"=>$search_text_title));


        }else{
            if($this->session->userdata('search') != NULL){
                $search_text = $this->session->userdata('search');

            }
            if($this->session->userdata('searchAuthor') != NULL){
                $search_text_author = $this->session->userdata('searchAuthor');

            }
            if($this->session->userdata('searchTitle') != NULL){
                $search_text_author = $this->session->userdata('searchTitle');

            }
        }
        $data['search'] = $search_text;
        $data['searchAuthor'] = $search_text_author;
        $data['searchTitle'] = $search_text_title;
        $data['publication_list'] = $this->publication_model->get_publication_list($search_text,$search_text_author,$search_text_title);
        $data['author_list'] = $this->author_model->get_author_list();


        $this->load->view('display_publications_view', $data);


    }


    //delete publication record from db
    function delete_publication($id)
    {
        //delete employee record
        $this->db->where('id_publication', $id);
        $this->db->delete('publication');
        return true;
        redirect('displaypublications/index');
    }

    function testDelete()
    {
        $con = mysqli_connect('localhost', 'root', '', 'sia');
        echo "<H1>Tests unitaires</H1>";
        $type = "test";
        $mdate = "test";
        $pubkey = "test";
        $title = "test";
        $pages = "test";
        $crossref = "test";
        $url = "test";
        $ee = "test";
        $publisher = "test";
        $isbn = "test";
        $series = "test";
        $booktitle = "test";
        $journal = "test";
        $volume = "test";
        $cdrom = "test";
        $number = "test";
        $year = "test";
        $sql = "INSERT INTO publication (type, title,year,pages,crossref, url, ee, publisher, isbn, series, booktitle, journal, volume, cdrom, number, mdate, pubkey)
    VALUES ('$type','$title', '$year', '$pages','$crossref', '$url', '$ee', '$publisher', '$isbn', '$series', '$booktitle', '$journal', '$volume', '$cdrom', '$number', '$mdate', '$pubkey')";
    $resultin = mysqli_query($con, $sql);
    $sqlmax = "SELECT MAX( id_publication ) FROM publication";
    $resultsel = mysqli_query($con, $sqlmax);
    $res = $resultsel->fetch_array(MYSQLI_NUM);
    $test =  $this->delete_publication($res[0]);
    $expected_result = TRUE;
    $test_name = "Test Delete"; 
    echo $this->unit->run($test,$expected_result,$test_name); 
    }


}
?>