<?php
/*
 * File Name: deleteemployee.php
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stats extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        //load the article model
        $this->load->model('stats_model');
        $this->load->library('unit_test');

    }

    //index function
    function index()
    {
        $data['mean_pub'] = $this->stats_model->get_mean_publication();
        $data['mean_pub_type'] = $this->stats_model->get_mean_publication_type();
        $data['mean_pub_auth'] = $this->stats_model->get_mean_publication_author();
        $data['mean_pub_auth_type'] = $this->stats_model->get_mean_publication_author_type();
        $data['mean_pub_year'] = $this->stats_model->get_mean_publication_year();
        $data['mean_pub_year_type'] = $this->stats_model->get_mean_publication_year_type();
        $data['mean_pub_year_auth'] = $this->stats_model->get_mean_publication_year_auth();
        $data['mean_pub_year_auth_type'] = $this->stats_model->get_mean_publication_year_auth_type();

        $this->load->view('stats_view', $data);


    }



}
?>