<?php
class home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'html'));
		$this->load->library('session');
		$this->load->library('unit_test');
	}
	
	function index()
	{
		$this->load->view('home_view');

	}
	
	function logout()
	{
		// destroy session
        $data = array('login' => '', 'uname' => '', 'uid' => '');
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        return true;
		redirect('home/index');
		
	}
	
	function testTU()
	{
		echo "<H1>Tests unitaires</H1>";
		$test = $this->logout();
		$expected_result = true;
		$test_name = "Test Logout";
		echo $this->unit->run($test,$expected_result,$test_name);
	}
	
	function testInsert()
	{
		$con = mysqli_connect('localhost', 'root', '', 'sia');
		echo "<H1>Tests unitaires</H1>";
		$type = "test";
		$mdate = "test";
		$pubkey = "test";
		$title = "test";
		$pages = "test";
		$crossref = "test";
		$url = "test";
		$ee = "test";
		$publisher = "test";
		$isbn = "test";
		$series = "test";
		$booktitle = "test";
		$journal = "test";
		$volume = "test";
		$cdrom = "test";
		$number = "test";
		$year = "test";
		$sql = "INSERT INTO publication (type, title,year,pages,crossref, url, ee, publisher, isbn, series, booktitle, journal, volume, cdrom, number, mdate, pubkey)
	VALUES ('$type','$title', '$year', '$pages','$crossref', '$url', '$ee', '$publisher', '$isbn', '$series', '$booktitle', '$journal', '$volume', '$cdrom', '$number', '$mdate', '$pubkey')";
		$test = $con->query($sql);
		$expected_result = TRUE;
		$test_name = "Test Insert";
		echo $this->unit->run($test,$expected_result,$test_name);
		$sql2 = "DELETE FROM publication WHERE type = 'test'";
		$con->query($sql2);
	}
}