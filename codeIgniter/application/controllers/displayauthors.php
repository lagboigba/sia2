<?php
/*
 * File Name: deleteemployee.php
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class displayauthors extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        //load the article model
        $this->load->model('author_model');
        $this->load->library('unit_test');

    }

    //index function
    function index()
    {
        $data['author_list'] = $this->author_model->get_author_list();

        $this->load->view('display_authors_view', $data);


    }





}
?>