<?php
class displaydetails extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','html'));
        $this->load->library('session');
        $this->load->database();
        $this->load->model('publication_model');
    }

//    function index()
//    {
//        $details = $this->publication_model->get_publication_by_id(65230);
//        $data['publication_id'] = $details[0]->publication_id;
//        $data['type'] = $details[0]->type;
//        $data['url'] = $details[0]->url;
//        $this->load->view('publication_view',$data);
//    }
//
    function display($pubkey){
        $pubkey  = base64_decode(str_replace('-', '=', str_replace('_', '/', $pubkey)));
        $details = $this->publication_model->get_publication_by_pubkey($pubkey);
        //var_dump($details);
        $data['title'] = $details[0]->title;
        $data['pages'] = $details[0]->pages;
        $data['year'] = $details[0]->year;
        $data['crossref'] = $details[0]->crossref;
        $data['url'] = $details[0]->url;
        $data['ee'] = $details[0]->ee;
        $data['publisher'] = $details[0]->publisher;
        $data['isbn'] = $details[0]->isbn;
        $data['series'] = $details[0]->series;
        $data['booktitle'] = $details[0]->booktitle;
        $data['journal'] = $details[0]->journal;
        $data['volume'] = $details[0]->volume;
        $data['cdrom'] = $details[0]->cdrom;
        $data['mdate'] = $details[0]->mdate;
        $data['pubkey'] = $details[0]->pubkey;




        $this->load->view('publication_view',$data);


    }
}