<html>
<head>
	<title>Upload Form</title>
</head>
<body>

	<h3>Your file was successfully uploaded!</h3>

	<ul>
		<?php foreach ($upload_data as $item => $value):?>
			<li><?php echo $item;?>: <?php echo $value;?></li>
		<?php endforeach; ?>
	</ul>

	<p><?php echo anchor('upload', 'Upload Another File!'); ?></p>

	<div id="visualiser">
		<?php  $name = $upload_data['file_name']; 
		$url_visu = base_url() . 'upload/displaypdf/'.$name;
		?>
		<a target="_blank" href="<?php echo $url_visu  ?>" >Afficher le document pdf</a>


	</div>
</body>
</html>