<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title ?></title>
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>">
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="nav navbar-nav navbar-right">
                <?php if ($this->session->userdata('login')){ ?>
                    <li><a href="<?php echo base_url(); ?>displaypublications">Return to list</a></li>
                    <li><a href="<?php echo base_url(); ?>displayauthors">View authors</a></li>
                    <li><a href="<?php echo base_url(); ?>publish">Publish something</a></li>
                    <li><a href="<?php echo base_url(); ?>stats">View stats</a></li>
                    <li><p class="navbar-text">Hello <?php echo $this->session->userdata('uname'); ?></p></li>
                    <li><a href="<?php echo base_url(); ?>home/logout">Log Out</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                    <li><a href="<?php echo base_url(); ?>signup">Signup</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4>Article Summary</h4>
            <hr/>
            <p>Title: <?php echo $title; ?></p>
            <p>Number of pages: <?php echo $pages; ?></p>
            <p>Year: <?php echo $year; ?></p>
            <p>Crossref: <?php echo $crossref; ?></p>
            <p>URL: <?php echo $url; ?></p>
            <p>EE: <?php echo $ee; ?></p>
            <p>Publisher: <?php echo $publisher; ?></p>
            <p>ISBN: <?php echo $isbn; ?></p>
            <p>Series: <?php echo $series; ?></p>
            <p>Booktitle: <?php echo $booktitle; ?></p>
            <p>Journal: <?php echo $journal; ?></p>
            <p>Volume: <?php echo $year; ?></p>
            <p>year: <?php echo $volume; ?></p>
            <p>CDROM: <?php echo $cdrom; ?></p>
            <p>mdate: <?php echo $mdate; ?></p>
            <p>pubkey: <?php echo $pubkey; ?></p>
            <p>PDF: </p>

            <div id="visualiser">
                <?php  $name = $pubkey;
                $url_visu = base_url() . 'upload/displaypdf/'.$name.'.pdf';
                ?>
                <a target="_blank" href="<?php echo $url_visu  ?>" >Afficher le document pdf</a>


            </div>




        </div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.10.2.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
</body>
</html>