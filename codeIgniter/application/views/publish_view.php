<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publish</title>
    <!--link the bootstrap css file-->
    <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
    <!-- link jquery ui css-->
    <link href="<?php echo base_url('assets/jquery-ui-1.11.2/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!--include jquery library-->
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>"></script>
    <!--load jquery ui js file-->
    <script src="<?php echo base_url('assets/jquery-ui-1.11.2/jquery-ui.min.js'); ?>"></script>

    <style type="text/css">
        .colbox {
            margin-left: 0px;
            margin-right: 0px;
        }
    </style>

    <script type="text/javascript">
        //load datepicker control onfocus
        $(function() {
            $("#publicationDate").datepicker();
        });
    </script>

</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="nav navbar-nav navbar-right">
                <?php if ($this->session->userdata('login')){ ?>
                    <li><a href="<?php echo base_url(); ?>publish">Publish something</a></li>
                    <li><a href="<?php echo base_url(); ?>displaypublications">View publications</a></li>
                    <li><a href="<?php echo base_url(); ?>displayauthors">View authors</a></li>
                    <li><a href="<?php echo base_url(); ?>profile">View profile</a></li>
                    <li><a href="<?php echo base_url(); ?>stats">View stats</a></li>
                    <li><p class="navbar-text">Hello <?php echo $this->session->userdata('uname'); ?></p></li>
                    <li><a href="<?php echo base_url(); ?>home/logout">Log Out</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                    <li><a href="<?php echo base_url(); ?>signup">Signup</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-sm-offset-3 col-lg-6 col-sm-6 well">
            <legend>Add Publication Details</legend>
            <?php
            $attributes = array("class" => "form-horizontal", "id" => "publication", "name" => "publicationform");
            echo form_open("publish/index", $attributes);?>
            <fieldset>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="type" class="control-label">Type</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <select id="type" name="type" value="<?php echo set_value('type'); ?>" class="form-control">
                                <option>Article</option>
                                <option>Book</option>
                                <option>Inclassification</option>
                                <option>Inproceedings</option>
                            </select>
                            <span class="text-danger"><?php echo form_error('type'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="title" class="control-label">Title</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="title" name="title" placeholder="title" type="text" class="form-control"  value="<?php echo set_value('title'); ?>" />
                            <span class="text-danger"><?php echo form_error('title'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="pages" class="control-label">Number of pages</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="pages" name="pages" placeholder="pages" type="text" class="form-control"  value="<?php echo set_value('pages'); ?>" />
                            <span class="text-danger"><?php echo form_error('pages'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="year" class="control-label">Year</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="year" name="year" placeholder="year" type="text" class="form-control"  value="<?php echo set_value('year'); ?>" />
                            <span class="text-danger"><?php echo form_error('year'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="crossref" class="control-label">Crossref</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="crossref" name="crossref" placeholder="crossref" type="text" class="form-control"  value="<?php echo set_value('crossref'); ?>" />
                            <span class="text-danger"><?php echo form_error('crossref'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="url" class="control-label">URL</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="url" name="url" placeholder="url" type="text" class="form-control"  value="<?php echo set_value('url'); ?>" />
                            <span class="text-danger"><?php echo form_error('url'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="ee" class="control-label">EE</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="ee" name="ee" placeholder="ee" type="text" class="form-control"  value="<?php echo set_value('ee'); ?>" />
                            <span class="text-danger"><?php echo form_error('ee'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="publisher" class="control-label">Publisher</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="publisher" name="publisher" placeholder="publisher" type="text" class="form-control"  value="<?php echo set_value('publisher'); ?>" />
                            <span class="text-danger"><?php echo form_error('publisher'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="isbn" class="control-label">ISBN</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="isbn" name="isbn" placeholder="isbn" type="text" class="form-control"  value="<?php echo set_value('isbn'); ?>" />
                            <span class="text-danger"><?php echo form_error('isbn'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="series" class="control-label">Series</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="series" name="series" placeholder="series" type="text" class="form-control"  value="<?php echo set_value('series'); ?>" />
                            <span class="text-danger"><?php echo form_error('series'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="booktitle" class="control-label">Booktitle</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="booktitle" name="booktitle" placeholder="booktitle" type="text" class="form-control"  value="<?php echo set_value('booktitle'); ?>" />
                            <span class="text-danger"><?php echo form_error('booktitle'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="journal" class="control-label">Journal</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="journal" name="journal" placeholder="journal" type="text" class="form-control"  value="<?php echo set_value('journal'); ?>" />
                            <span class="text-danger"><?php echo form_error('journal'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="volume" class="control-label">Volume</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="volume" name="volume" placeholder="volume" type="text" class="form-control"  value="<?php echo set_value('volume'); ?>" />
                            <span class="text-danger"><?php echo form_error('volume'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="cdrom" class="control-label">CDROM</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="cdrom" name="cdrom" placeholder="cdrom" type="text" class="form-control"  value="<?php echo set_value('cdrom'); ?>" />
                            <span class="text-danger"><?php echo form_error('cdrom'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="number" class="control-label">Number</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="number" name="number" placeholder="number" type="text" class="form-control"  value="<?php echo set_value('number'); ?>" />
                            <span class="text-danger"><?php echo form_error('number'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="mdate" class="control-label">Date</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="mdate" name="mdate" placeholder="mdate" type="text" class="form-control"  value="<?php echo set_value('mdate'); ?>" />
                            <span class="text-danger"><?php echo form_error('mdate'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-lg-4 col-sm-4">
                            <label for="pubkey" class="control-label">Publication Key</label>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                            <input id="pubkey" name="pubkey" placeholder="pubkey" type="text" class="form-control"  value="<?php echo set_value('pubkey'); ?>" />
                            <span class="text-danger"><?php echo form_error('pubkey'); ?></span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
                        <input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Insert" />
                        <input id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger" value="Cancel" />
                    </div>
                </div>




            </fieldset>
            <?php echo form_close(); ?>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
</body>
<?php echo($this->session->userdata('uid')) ?>
</html>
