<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publications</title>
    <!--link the bootstrap css file-->
    <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="nav navbar-nav navbar-right">
                <?php if ($this->session->userdata('login')){ ?>
                    <li><a href="<?php echo base_url(); ?>publish">Publish something</a></li>
                    <li><a href="<?php echo base_url(); ?>displayauthors">View authors</a></li>
                    <li><a href="<?php echo base_url(); ?>stats">View stats</a></li>
                    <li><a href="<?php echo base_url(); ?>profile">View profile</a></li>
                    <li><p class="navbar-text">Hello <?php echo $this->session->userdata('uname'); ?></p></li>
                    <li><a href="<?php echo base_url(); ?>home/logout">Log Out</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                    <li><a href="<?php echo base_url(); ?>signup">Signup</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table class="table table-striped table-hover">
                <thead>
                <tr class="bg-primary">
                    <th>#</th>
                    <th>Publication No</th>
                    <th>Type</th>
                    <th>Title</th>
                    <th>Year</th>
                    <th>Main Author</th>
                    <th>Details</th>
                    <th>Delete</th>

                </tr>
                </thead>
                <tbody>
                <form method='post' action="<?= base_url() ?>/displaypublications/index" >
                    Filtre by type<input type='text' name='search' value='<?= $search ?>'>
                    Filter by author<input type='text' name='searchAuthor' value='<?= $searchAuthor ?>'>
                    Filter by title<input type='text' name='searchTitle' value='<?= $searchTitle ?>'>
                    <input type='submit' name='submit' value='Submit'>
                </form>
                <?php for ($i = 0; $i < count($publication_list); $i++) { ?>
                    <tr>
                        <td><?php echo ($i+1); ?></td>
                        <td><?php echo $publication_list[$i]->id_publication; ?></td>
                        <td><?php echo $publication_list[$i]->type; ?></td>
                        <td><?php echo $publication_list[$i]->title; ?></td>
                        <td><?php echo $publication_list[$i]->year; ?></td>
                        <td><a href="<?php echo base_url() . "index.php/author/display/" . $author_list[$i]->id_author; ?>"><?=$author_list[$i]->fullName?></a></td>
                        <td><a href="<?php echo base_url() . "index.php/displaydetails/display/" . str_replace('=', '-', str_replace('/', '_', base64_encode($publication_list[$i]->pubkey))); ?>">Details</a></td>
                        <td><a href="<?php echo base_url() . "index.php/displaypublications/delete_publication/" . $publication_list[$i]->id_publication; ?>">Delete</a></td>

                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>