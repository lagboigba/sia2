<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Authors</title>
    <!--link the bootstrap css file-->
    <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="nav navbar-nav navbar-right">
                <?php if ($this->session->userdata('login')){ ?>
                    <li><a href="<?php echo base_url(); ?>publish">Publish something</a></li>
                    <li><a href="<?php echo base_url(); ?>displaypublications">View publications</a></li>
                    <li><a href="<?php echo base_url(); ?>profile">View profile</a></li>
                    <li><p class="navbar-text">Hello <?php echo $this->session->userdata('uname'); ?></p></li>
                    <li><a href="<?php echo base_url(); ?>home/logout">Log Out</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                    <li><a href="<?php echo base_url(); ?>signup">Signup</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table class="table table-striped table-hover">
                <thead>
                <tr class="bg-primary">
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($author_list); $i++) { ?>
                    <tr>
                        <td><?php echo $author_list[$i]->id; ?></td>
                        <td><?php echo $author_list[$i]->firstName; ?></td>
                        <td><?php echo $author_list[$i]->lastName; ?></td>
                        <td><a href="<?php echo base_url() . "index.php/author/display/" . $author_list[$i]->id; ?>">Details</a></td>

                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>